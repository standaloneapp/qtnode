# Contributors

* **[Luis Gustavo S. Barreto](https://github.com/gustavosbarreto)**

  * The author of this project

### The following contributions made indirectly.

* **[Artur Adib](https://github.com/arturadib)**

  * The node-qt codebase was used in some parts of this project

* **[Peter Tworek](https://github.com/tworaz)**

  * The integration of QRC and MOC of this project is based on their work on quicksilver project
